#include <stdio.h>

unsigned int ft_strlcpy(char *dest, char *src, unsigned int size);

int main()
{
    char str[] = "Test str1ng~";
    char dest[] = "t3St-STRiNG!";
    printf("Before: %s ->\n%s\n", str, dest);
    printf("Returns: %d\n", ft_strlcpy(dest, str, 10));
    printf("After: %s ->\n%s\n", str, dest);
}
