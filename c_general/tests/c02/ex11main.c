#include <stdio.h>

void ft_putstr_non_printable(char *str);

int main()
{
    char *str = "Coucou\ntu";
    printf("String: %s\n", str);
    printf("Function output: ");
    ft_putstr_non_printable(str);
    printf("\n");

    str = "\x01\x1F\x7F";
    printf("String: %s\n", str);
    printf("Function output: ");
    ft_putstr_non_printable(str);
    printf("\n");
}
