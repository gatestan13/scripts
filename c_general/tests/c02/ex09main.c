#include <stdio.h>

char *ft_strcapitalize(char *str);

int main()
{
    char str[] = "test str1ng~so f u n";
    printf("Before capitalize: %s\n", str);
    printf("Returns: %s\n", ft_strcapitalize(str));
    printf("After uppercase: %s", str);
}
