#include <stdio.h>

int ft_str_is_lowercase(char *str);

int main()
{
    char *str = "TEST";
    printf("Expected 0, returned %d\n", ft_str_is_lowercase(str));

    str = "low3r case";
    printf("Expected 0, returned %d\n", ft_str_is_lowercase(str));

    str = "`{";
    printf("Expected 0, returned %d\n", ft_str_is_lowercase(str));

    str = "lowercase";
    printf("Expected 1, returned %d\n", ft_str_is_lowercase(str));

    str = "";
    printf("Expected 1, returned %d\n", ft_str_is_lowercase(str));
}
