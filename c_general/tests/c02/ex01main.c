// Test case for c02ex01
#include <stdio.h>

char *ft_strncpy(char *dest, char *src, unsigned int n);

int main()
{
    char src[] = "String to copy", dest[] = "To be copied into";
    printf("Before: %s ->\n%s\n", src, dest);
    printf("After copying 10 characters, returns: %s\n", ft_strncpy(dest, src, 10));
    printf("After: %s ->\n%s\n", src, dest);

    return 0;
}
