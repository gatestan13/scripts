// Test case for c02ex00
#include <stdio.h>

char *ft_strcpy(char *dest, char *src);

int main()
{
    char src[] = "String to copy", dest[] = "To be copied into";
    printf("Before: %s\n%s\n", src, dest);
    printf("Returns: %s", ft_strcpy(dest, src));
    printf("After: %s\n%s\n", src, dest);
    return 0;
}
