#include <stdio.h>

char *ft_strupcase(char *str);

int main()
{
    char str[] = "Test str1ng~";
    printf("Before uppercase: %s\n", str);
    printf("Returns: %s\n", ft_strupcase(str));
    printf("After uppercase: %s", str);
}
