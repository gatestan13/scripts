#include <stdio.h>

char *ft_strlowcase(char *str);

int main()
{
    char str[] = "Test Str1ng~";
    printf("Before lowercase: %s\n", str);
    printf("Returns: %s\n", ft_strlowcase(str));
    printf("After lowercase: %s", str);
}
