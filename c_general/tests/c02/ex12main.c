#include <stdio.h>

void *ft_print_memory(void *addr, unsigned int size);

int main()
{
    char *contents = "Do what you want cause a pirate is free, you are a pirate!606264";
	char *contents2 = "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
    void *addr = contents;
	printf("Returns: %s\n", (char *)ft_print_memory(addr, 4));
	addr = contents2;
	printf("Returns: %s\n", (char *)ft_print_memory(addr, 4));
    return 0;
}
